#include <stdio.h>
#include <stdlib.h>
#include "moduly.h"

int wybor_opcji_programu()
{
    int opcja = 0;
    do
    {
        printf("\nWybierz opcje programu:\n");
        printf("1. Dodaj klienta\n");
        printf("2. Usun klienta\n");
        printf("3. Wyswietl ilosc klientow\n");
        printf("4. Wyswietl nazwiska klientow\n");
        printf("0. Zakoncz program\n");
        printf("-----------------------------------------------------\n");
        printf("Podaj odpowiednia cyfre symbolizujaca opcje programu: ");
        scanf("%d", &opcja);
    }
    while (!(opcja >= 0 && opcja <= 4));

    return opcja;

}

void dodaj_klienta(struct Client **listaKlientow)
{
    int ilosc = 0;
    do
    {
        printf("\nPodaj ilosc dodawanych klientow (od 1 do 10): ");
        scanf("%d", &ilosc);
    }
    while (ilosc < 1 || ilosc > 10);

    for (int i = 0; i < ilosc; i++)
    {
        char nazwisko[51] = "";
        printf("Podaj nazwisko klienta nr %d: ", i + 1);
        scanf("%50s", nazwisko);
        push_back(listaKlientow, nazwisko);
    }

    return;
}

void usun_klienta(struct Client **listaKlientow)
{
    char nazwisko[51] = "";
    printf("Podaj nazwisko klienta do usuniecia: ");
    scanf("%50s", nazwisko);
    pop_by_surname(listaKlientow, nazwisko);

    return;
}

void wyswietl_ilosc_klientow(struct Client *listaKlientow)
{
    int ilosc = list_size(listaKlientow);
    printf("\nIlosc zarejestrowanych klientow: %d\n", ilosc);

    return;
}

void wyswietl_nazwiska_klientow(struct Client *listaKlientow)
{
    printf("\nDostepna lista nazwisk klientow:\n");
    show_list(listaKlientow);

    return;
}

int main()
{
    struct Client *listaKlientow;
    listaKlientow = (struct Client *)malloc(sizeof(struct Client));
    listaKlientow = NULL;

    int opcja = 0;
    do
    {
        opcja = wybor_opcji_programu();
        switch (opcja)
        {
            case 1:
                dodaj_klienta(&listaKlientow);
                break;
            case 2:
                usun_klienta(&listaKlientow);
                break;
            case 3:
                wyswietl_ilosc_klientow(listaKlientow);
                break;
            case 4:
                wyswietl_nazwiska_klientow(listaKlientow);
                break;
        }
    }
    while (opcja != 0);

    return 0;
}
