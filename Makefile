CC=gcc
CLAGS=-Wall
LDLIBS=-lm

main: main.o klient.o moduły.o
    $(CC) $(CFLAGS) -o main main.o klient.o moduły.o $(LDLIBS)

main.o: main.c moduły.h
    $(CC) $(CFLAGS) -c main.c

klient.o: klient.c
    $(CC) $(CFLAGS) -c klient.c

moduły.o: moduły.c
    $(CC) $(CFLAGS) -c moduły.c


